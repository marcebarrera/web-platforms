const assert = require('assert');
const sumar = require('../app');
//afirmaciones

describe('Probar la suma de numeros',()=>{
  //afirmar que cinco mas cinco es 10
  it('cinco mas cinco es diez',()=>{
    assert.equal(10,sumar(5,5));
  });
  //afirmar que cinco mas cinco no son 55
  it('cinco mas cinco no es cincuenta y cinco',()=>{
    assert.notEqual("55",sumar(5,5));
  })

});