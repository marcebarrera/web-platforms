const Sequelize = require('sequelize');

const directorModel = require('./models/director');
const actorModel = require('./models/actor');
const bookingModel = require('./models/booking');
const copyModel = require('./models/copy');
const genreModel = require('./models/genre');
const memberModel = require('./models/member');
const movieModel = require('./models/movie');


const sequelize = new Sequelize('vc','root','pankesito',{
  host:'localhost',
  dialect:'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Actor = actorModel(sequelize,Sequelize);
const Booking = bookingModel(sequelize,Sequelize);
const Copy = copyModel(sequelize,Sequelize);
const Genre = genreModel(sequelize,Sequelize);
const Member = memberModel(sequelize,Sequelize);
const Movie = movieModel(sequelize,Sequelize);

//Foreign keys

//Movies
Director.hasMany(Movie,{ foreignKey: "director_id" });
Genre.hasMany(Movie, { foreignKey: "genre_id" });

//Copies
Movie.hasMany(Copy, { foreignKey: "movie_id" });

//Bookings
Member.hasMany(Booking, {foreignKey: "member_id" });
Copy.hasMany(Booking, { foreignKey: "copy_id" });

//Movies
Actor.belongsToMany(Movie, { through: 'movies_actors', foreignKey: "actor_id" } );
Movie.belongsToMany(Actor, { through: 'movies_actors', foreignKey: "movie_id" } );

sequelize.sync({
  force:true
}).then(()=>{
  console.log("DB Create");
});

module.exports = {
  Director,
  Actor,
  Booking,
  Copy,
  Genre,
  Member,
  Movie
};