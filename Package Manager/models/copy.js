module.exports = (sequelize, type) => {

  const Copy = sequelize.define('copy', {
    id:{type: type.INTEGER, primaryKey:true, autoIncrement: true},
    number:type.INT,
    format: type.ENUM,
    status: type.ENUM
  });

  return Copy;
};

