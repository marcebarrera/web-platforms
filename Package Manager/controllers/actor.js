const express = require('express');

const { Actor } = require('../db');


function create(req,res,next){
  let actor = new Object();
  actor.name = req.body.name;
  actor.lastName = req.body.lastName;

  Actor.create(actor).then(booking => res.status(200).json({
    message: "Actor creado",
    objs: actor
  })).catch( error => res.status(500).json({
    message: "No se pudo crear",
    objs: {}
  }));
}

function update(req, res, next) {
  let id = req.body.id;

  let actor = new Object();
  actor.id = req.body.id;
  actor.name = req.body.name;
  actor.lastName = req.body.lastName;

  Actor.update(actor, {
      where: {
          id: id
      }
  }).then(() => res.status(200).json({
      message: 'Actor actualizado',
      objs: actor
    })).catch(error => res.status(500).json({
      message: 'No se pudo actualizar',
      objs: {}
    }));
}

function list(req, res, next) {
  Actor.findAll().then(actors => res.status(200).json({
    message: 'Actores mostrados',
    objs: actors
  })).catch(error => res.status(500).json({
    message: 'No se pudieron mostrar los actores',
    objs: {}
  }));
}

function destroy(req, res, next) {
  let id = req.body.id;
  Actor.destroy({
      where: {
          id: id
      }
  }).then(() => res.status(200).json({
      message: 'Actor eliminado',
      objs: {
          id: parseInt(id)
      }
    })).catch(error => res.status(500).json({
      message: 'No se pudo eliminar al actor',
      objs: {}
    }));
    
}

function index(req, res, next) {
  let id = req.params.id;
  Actor.findByPk(id).then(actor => res.status(200).json({
      message: 'Actor mostrado',
      objs: actor
    })).catch(error => res.status(500).json({
      message: 'No se pudo mostrar al actor',
      objs: {}
    }));
}


module.exports = {
  list, create, update, destroy, index
}
