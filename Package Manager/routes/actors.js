const express = require('express');
const controller = require('../controllers/actors');

const router = express.Router();

router.post('/', controller.create);

router.get('/', controller.list);

router.get('/:id', controller.index);

router.put('/', controller.update);

router.delete('/', controller.destroy);

module.exports = router;
